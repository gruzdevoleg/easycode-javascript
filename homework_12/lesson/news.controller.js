const newsService = new NewsService();
const uiService = new NewsUI();
//const notification = new Notification();

// UI Elements
const form = document.forms['newsControlForm'];
const countrySelect = form['country'];
const categorySelect = form['category'];
const phraseInput = form['search'];

function onSelectChange(event) {
    const country = countrySelect.value;
    const category = categorySelect.value;

    if (!country || !category) return console.error('Введите страну и категорию для поиска');

    newsService.getTopHeadlinesNews(category, country, (response) => {
        const { totalResults, articles } = response;
        if (articles.length === 0) {
            uiService.clearContainer();
            uiService.newsContainer.insertAdjacentHTML('afterEnd', Notification.createNotificationTemplate());
        }
        // console.time();
        uiService.clearContainer();
        // console.timeEnd();

        // console.time();
        articles.forEach((article) => uiService.addArticle(article));
        // console.timeEnd();
    });
}

function onInputChange(event) {
    const searchPhrase = phraseInput.value;
    
    if (searchPhrase.length <= 3) return;

    newsService.getPhraseContainNews(searchPhrase, (response) => {
        const { totalResults, articles } = response;
        if (articles.length === 0) {
            uiService.clearContainer();
            uiService.newsContainer.insertAdjacentHTML('afterEnd', Notification.createNotificationTemplate());
        }
       
        // console.time();
        uiService.clearContainer();
        // console.timeEnd();

        // console.time();
        articles.forEach((article) => uiService.addArticle(article));
        // console.timeEnd();
    });
}


countrySelect.addEventListener('change', onSelectChange);
categorySelect.addEventListener('change', onSelectChange);
phraseInput.addEventListener('change', onInputChange);
phraseInput.addEventListener('keyup', onInputChange);
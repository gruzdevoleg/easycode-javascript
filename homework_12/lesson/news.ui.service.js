class NewsUI {
    constructor() {
        this.newsContainer = document.querySelector('.news-wrap .row');
    }

    /**
     * 
     * @param {Object} article 
     */
    addArticle(article) {
        // console.time();
        const template = NewsUI.generateArticleTemplate(article);
        // console.timeEnd();
        this.newsContainer.appendChild(template);
    }


    clearContainer() {
        let first = this.newsContainer.firstElementChild;
        while (first) {
            this.newsContainer.removeChild(first);
            first = this.newsContainer.firstElementChild;
        }
    }

    /**
     * 
     * @param {Object} article 
     */
    static generateArticleTemplate(article) {
        const articleBox = document.createElement('div');
        articleBox.classList.add('col', 's12', 'm6');

        const card = document.createElement('div');
        card.classList.add('card');

        const card_image = document.createElement('div');
        card_image.classList.add('card-image');

        const card_content = document.createElement('div');
        card_content.classList.add('card-content');

        const card_action = document.createElement('div');
        card_action.classList.add('card-action');

        const img = document.createElement('img');
        img.src = `${article.urlToImage}`;

        const card_title = document.createElement('span');
        card_title.classList.add('card-title');
        card_title.textContent = `${article.title || ''}`;

        const p = document.createElement('p');
        p.textContent = `${article.description || ''}`;

        const a = document.createElement('a');
        a.href = `${article.url || ''}`;
        a.target = '_blank';
        a.textContent = 'Read more';

        card_image.appendChild(img);
        card_content.appendChild(card_title);
        card_content.appendChild(p);
        card_action.appendChild(a);

        card.appendChild(card_image);
        card.appendChild(card_content);
        card.appendChild(card_action);
        articleBox.appendChild(card);
        return articleBox;
    }
}
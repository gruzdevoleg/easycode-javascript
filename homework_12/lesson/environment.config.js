class ENV {
    static get apiUrl() {
        return  'https://newsapi.org/v2';
    }

    static get apiKey() {
        return '0ba630db79394d45aabd00f26c0765ab';
    }

    static get country() {
        return 'ua';
    }
     
    static get category() {
        return 'technology';
    } 
}
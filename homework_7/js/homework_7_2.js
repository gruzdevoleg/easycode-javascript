//1. Найти в коде список ul и добавить класс “list”
const list = document.querySelector('ul');
list.classList.add('list');
//2. Найти в коде ссылку, находящуюся после списка ul, и добавить id=link
document.querySelector('ul').nextElementSibling.nextElementSibling.id = 'link';
//3. На li через один (начиная с самого первого) установить класс “item”
for (let i = 0; i < list.children.length; i += 2) list.children[i].classList.add('item');
//4. На все ссылки в примере установить класс “custom-link”
const allLinks = document.links; 
for (let item of allLinks) item.classList.add('custom-link');
//Код для задач брать со слайда 5.


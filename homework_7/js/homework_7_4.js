//1. В каждую ссылку, которая находятся внутри списка ul  добавить по тегу strong
// (в каждую ссылку один strong). 
const linksInList = document.querySelectorAll('ul a');
for (let link of linksInList) {
	let newStrongElem = document.createElement('strong');
	link.appendChild(newStrongElem);
	newStrongElem.textContent = `new strong element`;
}
//2. В начало документа (в начало body) добавить картинку img с атрибутами src и alt
//(текст придумайте сами). В src добавьте реальный url к картинке.
// Для создания элемента используйте метод createElement. 
const image = document.createElement('img');
image.src = 'img/new-image.jpg'; // вернет полный url, а не только “pic.gif” - http://site.com/pic.gif
image.alt = 'new picture'; // "text"
document.body.insertAdjacentElement('afterbegin', image);


//3. Найти на странице элемент mark, добавить в конец содержимого текст “green” 
//и на элемент установить класс green
const mark = document.querySelector('mark');
mark.insertAdjacentText('beforeend', 'green');
mark.classList.add('green');

//4. Отсортировать li внутри списка в обратном порядке (по тексту внутри)

const liInList = Array.from(list.children);
liInList.sort().reverse();
list.innerHTML = '';
for (let item of liInList) list.appendChild(item);


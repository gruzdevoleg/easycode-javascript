//1. Найти параграф и получить его текстовое содержимое (только текст!)
const paragraph = document.querySelector('p');
console.log(paragraph.textContent);

//2. Создать функцию, которая принимает в качестве аргумента узел DOM и возвращает
//информацию (в виде объекта) о типе узла, об имени узла и о количестве дочерних узлов
// (если детей нет - 0)
const getNodeInfo = (node) => {
	let nodeInfoObj = {
		type: node.nodeType,
		name: node.nodeName,
		childs: 0
	};
	if (node.childNodes.length > 0) nodeInfoObj.childs = node.childNodes.length;
	return nodeInfoObj;
};

console.log(getNodeInfo(document.querySelector('p')));

//3. Получить массив, который состоит из текстового содержимого ссылок 
//внутри списка: getTextFromUl(ul) ---> ["Link1", "Link2", "Link3"]
const getTextFromUl = (ul) => {
	let arrLinksText = [];
	const linksInUl = ul.getElementsByTagName('a');

	for (let link of linksInUl) arrLinksText.push(link.textContent);

	return arrLinksText;
};
console.log(getTextFromUl(document.querySelector('ul')));

//4. В параграфе заменить все дочерние текстовые узлы на “-text-” (вложенные теги должны остаться). Конечный результат:
//-text-<a href="#">reprehendunt</a>-text-<mark>nemore</mark>-text-
for (let i = 0; i < paragraph.childNodes.length; i++)
	if (paragraph.childNodes[i].nodeType === 3)
		paragraph.childNodes[i].data = '-text-';

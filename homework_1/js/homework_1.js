let string = 'some test string';

//получим 1-ю и последнюю букву строки (и выведем их в консоль)
const firstLetter = string[0];
const lastLetter = string[string.length-1];
console.log( firstLetter );
console.log( lastLetter );

//Сделаем 1-ю и последнюю букву в верхнем регистре
const ucFirstLetter = firstLetter.toUpperCase();
const ucLastLetter = lastLetter.toUpperCase();
let cutString = string.slice(1, string.length - 1);

console.log( ucFirstLetter + cutString + ucLastLetter);

//Найдем положение слова (подстроки) 'string' в строке
console.log( string.indexOf('string') );

//Найдем положение второго пробела
console.log( string.indexOf('string') - 1 );

//Получим строку с 5-го символа длиной 4 буквы
console.log( string.substr(4, 4) );// 5-й символ имеет индекс 4

//Получим строку с 5-го по 9-й символ
console.log( string.slice(4, 9) );

//Получим новую строку из исходной путем удаления из нее последних 6 символов
const newStr = string.slice(0, -6);
console.log( newStr );

//Из двух переменных a=20 и b=16 получить переменную string, в которой будет
//содержаться текст “2016”
const a = 20;
const b = 16;
string = String(a) + b;//!переменная string переопределена
console.log( string );// '2016'

//Получить число pi из Math и округлить его до 2-х знаков после точки
const pi = +Math.PI.toFixed(2) // 3.14
console.log( pi );

//Используя Math, найти максимальное и минимальное числа 
//из представленного ряда 15, 11, 16, 12, 51, 12, 13, 51
console.log( Math.max(15, 11, 16, 12, 51, 12, 13, 51) );
console.log( Math.min(15, 11, 16, 12, 51, 12, 13, 51) );

//a. Получить случайное число и округлить его до двух цифр после запятой
const rand_1 = Math.random();
console.log( +rand_1.toFixed(2) );
//b. Получить случайное целое число от 0 до X. X - любое произвольное число.
const max = 100; 
let rand = 0 - 0.5 + Math.random() * (max + 1);
rand = Math.round(rand);
console.log( rand );

//Проверить результат вычисления 0.6 + 0.7 - как привести к нормальному виду (1.3)?
const x = 0.6;
const y = 0.7;
//const sum = x + y;//1,29999999999998
const sum = (x * 10 + y * 10) / 10;//а так все хорошо
console.log( sum );

//Получить число из строки '100$'
const str = '100$';
const numFromString = +str.slice(0, -1);
console.log( numFromString );
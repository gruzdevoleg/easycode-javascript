//Создать класс “Пользователь” с базовыми свойствами “имя”, “дата регистрации” 
//и методом “получить информацию” (метод должен вывести имя и дату регистрации).
//Метод должен быть объявлен с помощью прототипов (Func.prototype...)
// Создать два наследника класса “Пользователь”: класс “Админ” и класс “Гость”.
//У класса “Админ” должно быть дополнительное свойство “суперАдмин” (может быть
//true/false, должно быть скрытым). Свойства определяются в момент вызова конструктора.
//У класса “Гость” должно быть свойство “срокДействия” (validDate, например),
//содержащее дату (например, одну неделю от момента регистрации).
//У классов-наследников метод “получить информацию” должен так же содержать информацию
// о дополнительных свойствах (“суперАдмин” и “срокДействия”)

//вспомогательный объект для форматирования даты при выводе
var options = {
  //era: 'long',
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  //weekday: 'long',
  //timezone: 'UTC',
 //hour: 'numeric',
  //minute: 'numeric',
  //second: 'numeric'
};

function User (name, dataRegister) {
	this.name = name;
	this.dataRegister = new Date(dataRegister);
}

User.prototype.getInfo = function() {
	console.log(`Имя: ${this.name}, Дата регистрации: ${this.dataRegister}`);
}

function Admin (name, dataRegister, superAdmin) {
	User.apply(this, arguments);
	this._superAdmin = superAdmin;
}

function Guest (name, dataRegister) {
	User.apply(this, arguments);
	this.validate = new Date(+this.dataRegister + (7*24*3600*1000));//7 дней после регистрации
}

//связываем прототипы наследников и родителей и не теряем родной конструктор
Admin.prototype = Object.create(User.prototype);
Admin.prototype.constructor = Admin;
Guest.prototype = Object.create(User.prototype);
Guest.prototype.constructor = Guest;

//в прототипах наследников переопределяем (а точнее создаем метод с таким же названием)
// метод getInfo но с другим функционалом
//теперь этот метод будет браться из прототипа наследника (раньше по цепочке), а не родителя
Admin.prototype.getInfo = function() {
	console.log(`Имя: ${this.name}, Дата регистрации: ${this.dataRegister.toLocaleString("ru", options)}, Суперадмин: ${this._superAdmin}`);
}

Guest.prototype.getInfo = function() {
	console.log(`Имя: ${this.name}, Дата регистрации: ${this.dataRegister.toLocaleString("ru", options)}, Срок действия: ${this.validate.toLocaleString("ru", options)}`);
}

var oleg = new Admin('Олег', '2019.02.12', true);
oleg.getInfo();

var elena = new Guest('Елена', '2019.02.22');
elena.getInfo();
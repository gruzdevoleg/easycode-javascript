//Создайте класс “Здание” (пусть у него будет имя, количество этажей,
//метод “получить количество этажей” и метод “установить количество этажей”).
//Создайте наследников этого класса:
//классы “Жилой дом” и “Торговый центр”. Используйте функциональное наследование 
//У жилого дома появится свойство “количество квартир на этаже”,
//а метод “получить количество этажей” должен вернуть объект вида 
//{этажи: 5, всегоКвартир: 5 * количествоКвартир}
//У торгового центра появится свойство “количество магазинов на этаже”, 
//а метод “получить количество этажей” должен вернуть объект вида {этажи: 3,
// всегоМагазинов: 3 * количествоМагазинов}
//От каждого класса создать экземпляр (дом, торговый центр)
function Building(name, floors) {
	this.name = name;
	this.floors = floors;
	this.getFloorsQuantity = () => this.floors;
	this.setFloorsQuantity = (userFloorsQuantity) => {
		this.floors = userFloorsQuantity;
	}
}

function ShoppingCenter(name, floors, shopsPerFloor) {
	Building.apply(this, arguments);
	this.shopsPerFloor = shopsPerFloor;
	this.getFloorsQuantity = () => ({
		floors: this.floors,
		totalShopsQuantity: this.floors * this.shopsPerFloor
	});
}

function House(name, floors, apartsPerFloor) {
	Building.apply(this, arguments);
	this.apartsPerFloor = apartsPerFloor;
	this.getFloorsQuantity = () => ({
		floors: this.floors,
		aparts: this.floors * this.apartsPerFloor
	});
}

var karavan = new ShoppingCenter('Караван', 3, 20);
var dominion = new House('Dominion', 24, 4);
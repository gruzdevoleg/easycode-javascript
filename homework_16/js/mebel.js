//Создать класс “Мебель” с базовыми свойствами “имя”, “цена” и методом “получить информацию”
// (метод должен вывести имя и цену). Метод должен быть объявлен с помощью прототипов 
//(Func.prototype...). Создать два экземпляра класса “Мебель”: экземпляр “ОфиснаяМебель” и
//“Мебель для дома”. Придумайте им по одному свойству, которые будут характерны только для
// этих экземпляров (например, для офисной мебели - наличие компьютерного стола или шредера).
//Метод “получить информацию” должен учитывать и добавленное вами новое свойство.
//Задача на переопределение метода у экземпляров класса.
function Furniture(name, price) {
	this.name = name;
	this.price = price;
}

Furniture.prototype.getInfo = function() {
	console.log(`Наименование: ${this.name}, Цена: ${this.price}`);
}

var officeFurniture = new Furniture('officeFurniture', '300$');
officeFurniture.additionalItem = 'Шредер';

var homeFurniture = new Furniture('homeFurniture', '500$');
homeFurniture.additionalItem = 'Подставка для ног';

officeFurniture.getInfo = function() {
	console.log(`Наименование: ${this.name}, Цена: ${this.price}, ${this.additionalItem} в комплекте`);
}

homeFurniture.getInfo = function() {
	console.log(`Наименование: ${this.name}, Цена: ${this.price}, ${this.additionalItem} в комплекте`);
}

officeFurniture.getInfo();
homeFurniture.getInfo();
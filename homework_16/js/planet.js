// Есть класс Planet
 function Planet(name) {
     this.name = name;
     this.getName = () => 'Planet name is ' + this.name;
}
// Создать наследника от Planet, который будет называться PlanetWithSatellite и будет
// принимать, кроме name, название спутника (satelliteName). Переопределите метод
// getName для PlanetWithSatellite так, чтобы он возвращал ту же самую строчку +
// дополнительный текст 'The satellite is' + satelliteName.
// Например:
// var earth = new PlanetWithSatellite('earth', 'moon');
// earth.getName(); // 'Planet name is earth. The satellite is moon’

function PlanetWithSatellite(name, satelliteName) {
	Planet.apply(this, arguments);
	this.satelliteName = satelliteName;
	var parentgetName = this.getName;
  	this.getName = () => parentgetName.call(this) + '. The satellite is ' + this.satelliteName;
 }

var earth = new PlanetWithSatellite('Earth', 'Moon');
console.log(earth.getName());
//Создать функцию, которая возвращает промис.  
//Функция принимает два аргумента - время, через которое промис должен выполниться,
// и значение, с которым промис будет выполнен. 

function promiseCreator(timeout, value) {
    return new Promise((res, rej) => {
        setTimeout(() => res(value), timeout);
        //setTimeout(() => rej(value), 1000);
   });
}

const prom = promiseCreator(2000, 'Ok!');
prom
	.then(console.log)
	.catch(console.log);
          
//Создать класс, который производит экземпляр со следующими свойствами:
//- promise - промис, который создается во время запуска конструктора;
//- reject - метод, при выполнении которого promise реджектится;
//- resolve - метод, при выполнении которого promise резолвится.
class Prom {
    constructor() {
        this.promise = new Promise((res, rej) => {
          this._res = res;
          this._rej = rej;
        });
    }

    resolve(res) {
        this._res(res);
    }
     
    reject(err) {
        this._rej(err);
    }
}
const inst = new Prom();
inst.promise
	.then(data => console.log(data))
	.catch(data => console.log(data));
inst.resolve('test');//test
//inst.reject('Something is wrong');//Something is wrong


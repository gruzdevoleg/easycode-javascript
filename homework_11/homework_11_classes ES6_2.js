//2.Реализовать конструктор и методы в ES6 синтаксисе:
/*function Component(tagName) {
  this.tagName = tagName || 'div';
  this.node = document.createElement(tagName);
}

Component.prototype.setText = function (text) { 
  this.node.textContent = text;
};*/
class Component_2 {
	constructor(tagName = 'div') {
		this.tagName = tagName;
  		this.node = document.createElement(tagName);
	}

	setText(text) {
		this.node.textContent = text;
	}
};

//3. Создать класс калькулятора который будет принимать стартовое значение
//и у него будут методы сложить, вычесть, умножить , разделить.
//Также у него должны быть геттер и сеттер для получения и установки текущего числа
//с которым производятся вычисления.

class Calculator {
	constructor(startValue) {
		this._result = startValue;
	}

	sum(value) {
		this._result = this._result += value;
	}

	minus(value) {
		this._result = this. _result -= value;
	}

	mul(value) {
		this._result = this._result *= value;
	}

	divide(value) {
		this._result = this._result /= value;
	}

	set currentValue(value) {
		this._result = value;
	}

	get currentValue() {
		return this._result;
	}
};

const calc = new Calculator(5);
calc.sum(5);//10
calc.minus(2);//8
calc.mul(2);//16
calc.divide(4);//4
console.log(calc.currentValue);//4
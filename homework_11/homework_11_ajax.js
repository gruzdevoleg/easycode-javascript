//Получить пользователей (users) от сервера https://jsonplaceholder.typicode.com
//используя класс созданный на занятии. Получив ответ от сервера вывести имена
//пользователей на страницу. При клике на имя пользователя в произвольном месте должна
//появиться подробная информация о нем. Для визуальной части можно использовать bootstrap
//или другие фреймворки. 
class UsersData {
	
    createUserDataList(url) {
    	let usersDataArray = [];
	    const outputUsersNames = (usersDataArray = []) => {
	    	let usersNamesListBox = document.querySelector('.user-name__list--wrapper');
       		let usersNamesList = document.createElement('ul');

	    	usersNamesList.classList.add('nav', 'flex-column', 'user-name__list', 'accordion');
	    	usersDataArray.forEach(function createList (user) {
	    		if (!Object.keys(user)) return;
	    		let {
    				name = 'No data',
    				username = 'No data',
    				address: {
    					street = 'No data', 
    					suite = 'No data',
    					city = 'No data',
    					zipcode = 'No data',
    					geo: {
    						lat = 'No data',
    						lng = 'No data'
    					}
    				},
    				company: {
    					name: company_name = 'No data',
    					catchPhrase = 'No data',
    					bs = 'No data'
    				},
    				email = 'No data',
    				phone = 'No data',
    				website = 'No data',
    				id = 'No data'
    			} = user;

    			//список имен
    			let newUserItem = document.createElement('li');
				newUserItem.classList.add('user-name__item');
    			newUserItem.textContent = name;
    			usersNamesList.appendChild(newUserItem);
	    			    			
				//создаем вложенный список с остальной инфо
    			let userInfoList = document.createElement('ul');
    			userInfoList.classList.add('user-info__list', 'dropdown-list');
    			userInfoList.innerHTML = `
    				<li class='user-info__item dropdown-item'>Username: ${username}</li>
					<li class='user-info__item dropdown-item'>Street: ${street}</li>
					<li class='user-info__item dropdown-item'>Suite: ${suite}</li>
    				<li class='user-info__item dropdown-item'>City: ${city}</li>
					<li class='user-info__item dropdown-item'>Zipcode: ${zipcode}</li>
					<li class='user-info__item dropdown-item'>Lat: ${lat}</li>
					<li class='user-info__item dropdown-item'>Lng: ${lng}</li>
					<li class='user-info__item dropdown-item'>Company Name: ${company_name}</li>
					<li class='user-info__item dropdown-item'>Catch phrase: ${catchPhrase}</li>
					<li class='user-info__item dropdown-item'>Bs: ${bs}</li>
					<li class='user-info__item dropdown-item'>Email: ${email}</li>
    				<li class='user-info__item dropdown-item'>Phone: ${phone}</li>
					<li class='user-info__item dropdown-item'>Website: ${website}</li>
					<li class='user-info__item dropdown-item'>Id: ${id}</li>
				`;
    			newUserItem.appendChild(userInfoList);
	    	});
	    	usersNamesListBox.insertBefore(usersNamesList, usersNamesListBox.firstChild);
	    	usersNamesList.addEventListener('click', function(event) {
	    		const accordionItems = usersNamesList.children;
			    let target = event.target;
			    while (target.tagName !== 'LI') {
					target = target.parentElement;
				}
				target.classList.toggle('active');
				for (let item of accordionItems)
					if (item !== target && item.classList.contains('active'))
						item.classList.remove('active');
			});
	    };

        const xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.send();
        xhr.addEventListener('load', function() {
        	usersDataArray = JSON.parse(this.responseText);
        	outputUsersNames(usersDataArray);
		});
    }
};

const usersData = new UsersData();
usersData.createUserDataList('https://jsonplaceholder.typicode.com/users');
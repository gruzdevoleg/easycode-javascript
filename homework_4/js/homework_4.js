//1/. Создать функцию multiply, которая будет принимать любое количество чисел
// и возвращать их произведение: multiply(1,2,3) = 6 (1*2*3)
//Если нет ни одного аргумента, вернуть ноль: multiply() // 0
function multiply() {
	let mul = 1;

	for (let i = 0; i < arguments.length; i++) {
		mul *= arguments[i]; 
	}
	if (arguments.length > 0) {
		return mul;
	} else {
		return 0;
	}
}

console.log(multiply(5,2,3,8));

//2. Создать функцию, которая принимает строку и возвращает строку-перевертыш:
// reverseString(‘test’) // “tset”.
function reverseString(string) {
	let reverse = '';
	for (let i = string.length-1; i >= 0; i--) {
		reverse = reverse + string[i];
	}
	return reverse;
}

console.log(reverseString('test'));

//3 Создать функцию, которая в качестве аргумента принимает строку из букв и 
//возвращает строку, где каждый символ разделен пробелом и заменен на юникод-значение символа: 
//getCodeStringFromText(‘hello’) // “104 101 108 108 111” 
//подсказка: для получения кода используйте специальный метод charCodeAt()

function getCodeStringFromText(string) {
	let unicodeString = '';

	for (let i = 0; i < string.length; i++ ) {
		unicodeString += string.charCodeAt(i) + ' '; 
	}
	//удалим последний пробел из полученной	строки
	unicodeString = unicodeString.slice(0, -1);
	return unicodeString;
}

console.log(getCodeStringFromText('hello'));

//4 Создать функцию угадай число. Она принимает число от 1-10 
//(обязательно проверить что число не больше 10 и не меньше 0).
//Генерирует рандомное число от 1-10 и сравнивает с переданным числом.
//Если они совпали то возвращает “Вы выиграли” если нет то
// “Вы не угадали ваше число 8 а выпало число 5”.
// Числа в строке указаны как пример вы подставляете реальные числа.
function guessNumber(userNumber) {
	function randomInteger(min, max) {
	    let rand = min - 0.5 + Math.random() * (max - min + 1)
	    rand = Math.round(rand);
	    return rand;
	 }

	if (typeof userNumber == 'number' && userNumber >= 1 && userNumber <= 10) {
		let randomNumber = randomInteger(1, 10);
		if (userNumber === randomNumber) {
		 	return 'Вы выиграли';
		} else {
		 	return `Вы не угадали. Ваше число ${userNumber}, а выпало число ${randomNumber}`;
		}
	} else {
		return 'Вы ввели неверные данные';
	}
}

console.log(guessNumber(5));

//5. Создать функцию, которая принимает число N и возвращает массив,
//заполненный числами от 1 до N: getArray(10); // [1,2,3,4,5,6,7,8,9,10]
function getArray(n) {
	let i = 1;
	let arr = [];

	while (i <= n) {
		arr.push(i);
		i++;
	}
	return arr;
}

console.log(getArray(10));

//6. Создать функцию, которая принимает массив, а возвращает новый массив
//с дублированными элементами входного массива:
//doubleArray([1,2,3]) // [1,2,3,1,2,3]
function doubleArray(arr) {
	let doubleArr = arr.concat(arr.slice());
	return doubleArr;
}

console.log(doubleArray([1,2,3]));

//7. Создать функцию, которая принимает произвольное (любое) число массивов
//и удаляет из каждого массива первый элемент, а возвращает массив из оставшихся значений: 
//changeCollection([1,2,3], [‘a’, ’b’, ‘c’]) → [ [2,3], [‘b’, ‘c’] ], changeCollection([1,2,3]) → [ [2,3] ] и т.д.
function changeCollection() {
	let changedArray = [];
	for (let i = 0; i < arguments.length; i++) {
		arguments[i].shift();
		changedArray.push(arguments[i]);
	}
	return changedArray;
}
console.log(changeCollection([1,2,3], ['a', 'b', 'c'], ['Oleg', 'Igor', 'Elena']));

//8. Создать функцию которая принимает массив пользователей,
//поле на которое хочу проверить и значение на которое хочу проверять. 
//Проверять что все аргументы переданы. Возвращать новый массив с пользователями
//соответсвующие указанным параметрам.
//funcGetUsers(users, “gender”, “male”); 
//[ {name: “Denis”, age: “29”, gender: “male”} , {name: “Ivan”, age: “20”, gender: “male”} ]

function funcGetUsers(usersArr, key, value) {
	let sortUsersArr = [];
	if (usersArr && key && value) {
		//т.к. forEach прошли только сегодня, то сделал через for
		for (let i = 0; i < usersArr.length; i++) {
			if (usersArr[i][key] === value) {
				sortUsersArr.push(usersArr[i]);
			}
		}
		if (sortUsersArr.length > 0) {
			return sortUsersArr;
		} else {
			return 'Пользователей, удовлетворяющих условию, нет';
		}

	} else {
		return 'Введите все параметры';
	}
}

let users = [
	{
		name: 'Denis',
		age: '29',
		gender: 'male'
	},
	{
		name: 'Ivan',
		age: '20',
		gender: 'male'
	},
	{
		name: 'Elena',
		age: '30',
		gender: 'female'
	}
];

console.log(funcGetUsers(users, 'gender', 'male'));
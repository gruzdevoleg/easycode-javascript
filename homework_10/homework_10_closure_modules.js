//1.Создайте функцию которая бы умела делать:
/*minus(10)(6); // 4
minus(5)(6); // -1
minus(10)(); // 10
minus()(6); // -6
minus()(); // 0
Подсказка, функция minus должна возвращать другую функцию.*/
const minus = (value_1 = 0) => (value_2 = 0) => value_1 - value_2;
console.log(minus(10)(6));//4
console.log(minus(5)(6));//-1
console.log(minus(10)()); // 10
console.log(minus()(6)); // -6
console.log(minus()()); // 0

//2.Реализовать функцию, которая умножает и умеет запоминать возвращаемый
// результат между вызовами:
//function MultiplyMaker ...
//const multiply = MultiplyMaker(2);
//multiply(2); // 4 (2 * 2)
//multiply(1); // 4 (4 * 1)
//multiply(3); // 12 (4 * 3)
//multiply(10); // 120 (12 * 10)
const MultiplyMaker = (num_1 = 0) => {
	let currentRes = num_1; 
	return (num_2 = 0) => {
		currentRes *= num_2;
		return currentRes;
	};
};
const multiply = MultiplyMaker(2);
console.log(multiply(2)); // 4 (2 * 2)
console.log(multiply(1)); // 4 (4 * 1)
console.log(multiply(3)); // 12 (4 * 3)
console.log(multiply(10)); // 120 (12 * 10)

//3. Реализовать модуль, который работает со строкой и имеет методы:
//a. установить строку
// - если передано пустое значение, то установить пустую строку
// - если передано число, число привести к строке
//b. получить строку
//c. получить длину строки
//d. получить строку-перевертыш
/*Пример:
модуль.установитьСтроку(‘abcde’);
модуль.получитьСтроку(); // ‘abcde’
модуль.получитьДлину(); // 5*/
const stringHandler = ( () => {
	let string;
	
	return {
        setString: (newString = '') => {
			string = (newString === '') ? '' : newString;
			string = (typeof newString === 'number') ? toString(newString) : newString;  
		},
        getString: () => string,
        getStringLength: () => string.length,
        getReverseString: () => string.split('').reverse().join('')
	};

})();

stringHandler.setString('ewwqetgqrhqheer');
console.log(stringHandler.getString());
console.log(stringHandler.getStringLength());
console.log(stringHandler.getReverseString());

//4.Создайте модуль “калькулятор”, который умеет складывать, умножать, вычитать,
//делить и возводить в степень. Конечное значение округлить до двух знаков 
//после точки (значение должно храниться в обычной переменной, не в this).
//модуль.установитьЗначение(10); // значение = 10
//модуль.прибавить(5); // значение += 5
//модуль.умножить(2); // значение *= 2
//модуль.узнатьЗначение(); // вывести в консоль 30 (здесь надо округлить)
//Также можно вызывать методы цепочкой:
//модуль.установитьЗначение(10).вСтепень(2).узнатьЗначение(); // 100
const calculator = ( () => {
	let currentValue;
	
	return {
        setValue: function (value) {
			currentValue = value; 
			return this;
		},
        sum: function (value) {
			currentValue += value;
			return this;  
		},
	    multiple: function (value) {
			currentValue *= value;
			return this;
		},
        divide: function (value) {
			currentValue /= value;
			return this;  
		},
        pow: function (value) {
			currentValue = Math.pow(currentValue, value);
			return this;
		},
        getResult: () => console.log(currentValue.toFixed(2))
	};
})();

calculator.setValue(10).sum(5).multiple(2).pow(2).divide(4).getResult();//225.00

//1.Создать конструктор для производства автомобилей. Конструктор должен принимать
//марку автомобиля и возраст машины. Конструктор должен иметь метод, 
//который возвращает марку, и второй метод, который возвращает год производства машины
//(год текущий минус возраст машины, использовать Date для получения текущего года)
//Марка машины всегда должна возвращаться с большой буквы!
function Car(name = 'Unknown', age = 'Unknown') {
	this.name = name;
	this.age = age;
	this.getName = () => this.name.charAt(0).toUpperCase() + this.name.slice(1);
	this.getProduceYear = () => new Date().getFullYear() - this.age;
}

let lexus = new Car('lexus', 2);
console.log(lexus.getName()); // 'Lexus'
console.log(lexus.getProduceYear()); // 2017 (2019-2);

//2. Написать конструктор, который умеет элементарно шифровать строки
//(например, сделать из строки строку-перевертыш, или заменить все символы их цифровым
//представлением, или любой другой метод). Конструктор при инициализации получает строку 
//и имеет следующие методы:
//a. показать оригинальную строку
//b. показать зашифрованную строку
//Строки не должны быть доступны через this, только с помощью методов.

function Encryption(string = '') {
	this.createReverseString = () => string.split('').reverse().join('');
	this.showOriginalString = () =>	console.log(string);
	this.showReverseString = () => console.log(this.createReverseString());
};

const encrypt_1 = new Encryption('weywhthtjwjtbgsdh');
encrypt_1.showOriginalString();
encrypt_1.showReverseString();
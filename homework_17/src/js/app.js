import { LoginComponent } from './components/login.component';
import { SignupComponent } from './components/signup.component';
import { HomeComponent } from './components/home.component';
import { NotFoundComponent } from './components/notfound.component';

//объект с возможными роутами
const routes = {
    '/': new HomeComponent(),
    '/login': new LoginComponent(),
    '/signup': new SignupComponent(),
    '**': new NotFoundComponent()//если любой другой путь
};

//функция, которая в зависимости от роута вставляет соответствующий компонент
const router = () => {
    const container = document.querySelector('app-container');
    
    //узнаем на каком урле находится пользователь
    const url = location.hash.slice(1).toLowerCase() || '/';
    
    //берем из routes компонент, соответствующий текущему урлу
    const component = routes[url] || routes['**']; 
    //и вставляем в контейнер на страницу через innerHTML контейнера
    //(метод render возвращает разметку)
    container.innerHTML = component.render();
    component.afterRender();
};

//вешаем эту функцию как обработчик на событие загрузки/перезагрузки страницы и изменение урла
window.addEventListener('load', router);
window.addEventListener('hashchange', router);




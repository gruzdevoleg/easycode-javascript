export class Http {
    post(url, data, options) {
        return new Promise((resolve, reject) => {
            //вместо чистого Ajax (new XMLHttpRequest, open, send...)
            //используем fetch
            fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json'
                }
            })
            .then((response) => response.json())// или JSON.parse(response)
            .then((data) => resolve(data))
            .catch((err) => reject(err));
        });
    }
}

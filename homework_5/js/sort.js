//1. Отсортируйте массив массивов так, чтобы вначале располагались наименьшие массивы
//(размер массива определяется его длиной): [  [14, 45],  [1],  ['a', 'c', 'd']  ] → [ [1], [14, 45], ['a', 'c', 'd'] ];
let arrToSort = [ [14, 45], [1], ['a', 'c', 'd'], ['Oleg', 'Ivan', 'Olga', 'Elena'], [] ];
function compareArrLength(a, b) {
  return a.length - b.length;
}
let sortedArr = arrToSort.sort(compareArrLength);
console.log(sortedArr);

//2. Есть массив объектов:
let cpuArr = [
    {cpu: 'intel', info: {cores:2, сache: 3}},
    {cpu: 'intel', info: {cores:4, сache: 4}},
    {cpu: 'amd', info: {cores:1, сache: 1}},
    {cpu: 'intel', info: {cores:3, сache: 2}},
    {cpu: 'amd', info: {cores:4, сache: 2}}
];
//Отсортировать их по возрастающему количеству ядер (cores).
function compareCores(a, b) {
  return a.info.cores - b.info.cores;
}
let cpuSortArr = cpuArr.sort(compareCores);
console.log(cpuSortArr);
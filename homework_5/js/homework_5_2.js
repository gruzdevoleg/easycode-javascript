//1. На основе массива [1,2,3,5,8,9,10] сформировать новый массив,
//каждый элемент которого будет хранить информацию о числе и его четности:
//[{digit: 1, odd: true}, {digit: 2, odd: false}, {digit: 3, odd: true}...]
let numbersArr = [1,2,3,5,8,9,10];
let newnumbersArr = [];
numbersArr.forEach(function (item, i) {
	newnumbersArr[i] = {};
	newnumbersArr[i].digit = item;
	newnumbersArr[i].odd = !!(item % 2);
});
console.log(newnumbersArr);

//2. Проверить, содержит ли массив [12, 4, 50, 1, 0, 18, 40] элементы, равные нулю.
//Если да - вернуть false.
let checkZeroArr = [12, 4, 50, 1, 0, 18, 40];
function isZeroInArray() {
	function isZero(number) {
	  return number === 0;
	}

	return !checkZeroArr.some(isZero);
}

console.log(isZeroInArray());

//3. Проверить, содержит ли массив ['yes', 'hello', 'no', 'easycode', 'what'] 
//хотя бы одно слово длиной больше 3х букв. Если да - вернуть true
let checkWordsArr = ['yes', 'hello', 'no', 'easycode', 'what'];
function isLongWord() {
	function checkWordLength(word) {
	  return word.length > 3;
	}

	return checkWordsArr.some(checkWordLength);
}

console.log(isLongWord());


//4.Дан массив объектов, где каждый объект содержит информацию о букве и месте её положения
//в строке {буква: “a”, позиция_в_предложении: 1}:

const lettersArray = [
	{
		char:"a",
		index:12
	},
	{
		char:"w",
		index:8
	},
	{
		char:"Y",
		index:10
	},
	{
		char:"p",
		index:3
	},
	{
		char:"p",
		index:2
	},
	{
		char:"N",
		index:6
	},
	{
		char:" ",
		index:5
	},
	{
		char:"y",
		index:4
	},
	{
		char:"r",
		index:13
	},
	{
		char:"H",
		index:0
	},
	{
		char:"e",
		index:11
	},
	{
		char:"a",
		index:1
	},
	{
		char:" ",
		index:9
	},
	{
		char:"!",
		index:14
	},
	{
		char:"e",
		index:7
	}
];

//Напишите функцию, которая из элементов массива соберет и вернёт
//строку, основываясь на index каждой буквы. Например:
//[{char:"H",index:0}, {char:"i",index: 1}, {char:"!",index:2}] → “Hi!”

//Подсказка: вначале отсортируйте массив по index, затем используйте reduce() для построения
//строки
function createString(arr) {
	//функция для сортировки по индексу
	function compareIndex(a, b) {
	  return a.index - b.index;
	}

	const sortArr = arr.sort(compareIndex);
	let string = sortArr.reduce(function (sumString, current) {
		return sumString + current.char;
	}, '');

	return string;
}

console.log(createString(lettersArray));
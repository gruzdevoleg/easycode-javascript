//1. Создать две функции и дать им осмысленные названия:
//- первая функция принимает массив и колбэк (одна для всех вызовов)
//- вторая функция (колбэк) обрабатывает каждый элемент массива
//(для каждого вызова свой callback)

//Первая функция возвращает строку “New value: ” и результат обработки:
//firstFunc([‘my’, ‘name’, ‘is’, ‘Trinity’], handler1) → “New value: MyNameIsTrinity”
//firstFunc([10, 20, 30], handler2) → “New value: 100, 200, 300,”
//firstFunc([{age: 45, name: ‘Jhon’}, {age: 20, name: ‘Aaron’}], handler3) →
//“New value: Jhon is 45, Aaron is 20,”
//firstFunc([‘abc’, ‘123’], handler4) → “New value: cba, 321,” // строки инвертируются
//Подсказка: secondFunc должна быть представлена функцией, которая принимает
//один аргумент (каждый элемент массива) и возвращает результат его обработки
function arrayConversion(array, elemArrTrasform) {
	let string = '';
	array.forEach( function (item) {
		string += elemArrTrasform(item);
	});
	//если последний символ полученной строки пробел, то удаляем его
	if (string[string.length-1] === ' ') {
		string = string.trim();
	}
	return `New value: ${string}`;
}

//callback handler_1 делает первую букву большой
const handler_1 = function (item) {
	return item.charAt(0).toUpperCase() + item.slice(1);
};
console.log(arrayConversion(['my', 'name', 'is', 'Trinity'], handler_1));

//callback handler_2 умножает каждый элемент на 10 и преобразует в строку
const handler_2 = function (item) {
	return (item * 10) + ', ';
};
console.log(arrayConversion([10, 20, 30], handler_2));

//callback handler_3 возвращает из каждого объекта (элемента массива) имя пользователя
//и его возраст. И возвращает в виде строки
const handler_3 = function (item) {
	return item.name + ' is ' + item.age + ', ';
};
console.log(arrayConversion([{age: 45, name: 'Jhon'}, {age: 20, name: 'Aaron'}], handler_3));

//callback handler_4 возвращает инвертированный элемент массива
const handler_4 = function (item) {
	return item.split('').reverse().join('') + ', ';
};
console.log(arrayConversion(['abc', '123'], handler_4));


//2. Написать аналог метода every. Создайте функцию every, она должна принимать
// первым аргументом массив чисел (обязательно проверьте что передан массив)
//вторым аргументом callback 
//функция должна возвращать true или false в зависимости от результата вызова callback
// (проверить число больше 5).
// Callback  должен принимать один элемент массива, его индекс в массиве и весь массив. 
function every(arr, handler) {
	if (typeof arr === 'object' && arr.length > 0) {
		for (let i = 0; i < arr.length; i++) {
			if (handler(arr[i]) !== true) {
				return false;
			}
		}
		return true;
	} else {
		return 'Первым аргументом функции должен быть массив';
	}
}

//функция-callback, которая проверяет элемент массива
const check = function (item, i, arr) {
	return (item > 5) ? true : false;
};

console.log(every([10, 2, 3, 8, 4, 6], check));//false
console.log(every([10, 6, 7, 8, 12, 6], check));//true
console.log(every('ffhsghsg', check));//Первым аргументом функции должен быть массив
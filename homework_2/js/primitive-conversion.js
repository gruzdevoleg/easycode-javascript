//Создать объект с полем product, равным ‘iphone’
let item = {
	product: 'iphone',
};

//Добавить в объект поле price, равное 1000 и поле currency, равное ‘dollar’
item.price = 1000;
item.currency = 'dollar';

//Добавить поле details, которое будет содержать объект с полями model и color
//Если все поля добавлять по очереди, не создавая сразу готовый объект со всеми полями,
//то тогда так: 
item.details = {};//создали поле details, равное пустому объекту
item.details.model = 'X';//добавили в него свойство model
item.details.color = 'black';//добавили в него свойство color

console.log( item.details.model );

//Чему равно а, почему?

let a = 0 || 'string';//'string'. 
//Объяснение: преобразование 0->false, 'string'->true.
//Логический оператор || запнется на правде и вернет
//оригинальное значение операнда, на котором запнулся, т.е 'string'
console.log( a );

a = 1 && 'string';//'string'. 
//Объяснение: преобразование 1->true, 'string'->true.
//Логический оператор && дойдет до конца выражения (т.к. не встретит ложь) и вернет
//оригинальное значение операнда, на котором запнулся, т.е 'string'
console.log( a );

a = null || 25;//25. 
//Объяснение: преобразование null->false, 25->true. Логический оператор || запнется
//на правде (true), т.е. дойдет до конца выражения и вернет оригинальное значение операнда,
//на котором запнулся, т.е 25
console.log( a );

a = null && 25;//null.
//Объяснение: логическое преобразование null->false. Логический оператор && сразу же запнется
//на лжи (false) и вернет оригинальное значение операнда, на котором запнулся, т.е null
console.log( a );

a = null || 0 || 35;//35
//Объяснение:
//преобразование null->false, 0->false, 35->true. Таким образом оператор ||
// дойдет до конца выражения (т.к. не встретит ранее true) и вернет оригинальное
//значение операнда, на котором запнулся, т.е 35
console.log( a );

a = null && 0 && 35;//null
//Объяснение: 
//преобразование null->false. Таким образом оператор &&
//сразу же запнется на false и вернет оригинальное значение операнда,
//на котором запнулся, т.е null
console.log( a );


//Что отобразится в консоли. Почему?

console.log(12 + 14 + '12');// '2612'.
 //Объяснение: 12+14=26. Т.к следующее слагаемое '12' строкового типа, то 
 //выполнится строковое преобразование. Число 26 преобразуется в строку '26'
 //и получим: '26'+'12'='2612'

console.log(3 + 2 - '1');//4
//Объяснение: 
//1 действие 3+2=5.
//2 действие. Далее выполнится численное преобразование '1'->1, и получим: 5-1=4 

console.log( '3' + 2 - 1 );//31
//Объяснение: 
// 1 действие: '3' + 2 = '32', т.к при сложении со строкой выполнится строковое преобразование
//2->'2'
// 2 действие: '32' - 1 = 31, здесь выполнится численное преобразование '32'-> 32, т.к. это
//матем. выражение (но не сложение строк или со строкой) и получим: 32-1=31


console.log( true + 2 );//3
//Объяснение: это математическое выражение (но не сложение строк или со строкой)
//значит выполнится численное преобразование true->1, получим 1+2=3

console.log( +'10' + 1 );//11
//Объяснение: 
//унарный '+' преобразует строку '10' в число 10, получим 10+1=11


console.log( undefined + 2 );//NaN
//Объяснение: здесь тоже численное преобразование
//undefined->NaN, получим NaN+2=NaN (NaN с чем либо дает NaN)

console.log( null + 5 );//5
//Объяснение: здесь тоже численное преобразование null → 0, получим 0+5=5


console.log( true + undefined );//NaN
//Объяснение: здесь тоже численное преобразование
//undefined->NaN, а NaN с чем либо дает NaN
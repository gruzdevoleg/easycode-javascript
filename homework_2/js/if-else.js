//1. Если переменная равна “hidden”, присвоить ей значение “visible”, иначе - “hidden”.
let x = 5;

if (x == 'hidden') {
	x = 'visible';
} else {
	x = 'hidden';
}
console.log(x);

//2. Используя if, записать условие:
 //если переменная равна нулю, присвоить ей 1;
//если меньше нуля - строку “less than zero”;
//если больше нуля - используя оператор “присвоение”, переменную умножить на 10 (использовать краткую запись).
let b = 7;

if (b === 0) {
	b = 1;
} else if (b < 0){
	b = 'less than zero';
} else {
	b *= 10;
}
console.log(b);

//3. Дан объект 
let car = {
	name: 'Lexus',
	age: 10,
	create: 2008,
	needRepair: false
};
//Написать условие если возраст машины больше 5 лет то нужно вывести в консоль 
//сообщение 'Need Repair' и свойство needRepair в объекте car изменить на true; иначе изменить на false.

if (car.age > 5) {
	console.log('Need Repair');
	car.needRepair = true;
} else {
	car.needRepair = false;
}

//4. Дан объект 
let productItem = {
	name: 'Intel core i7',
	price: '100$',
	discount: '15%'
};

//Написать условие если у productItem есть поле discount и там есть значение то в объекте productItem
//создать поле priceWithDiscount и записать туда цену с учетом скидки и вывести ее в консоль,
//обратите внимание  что поля discount и price это строки и вам из них нужно получить числа,
//чтобы выполнить расчет. иначе если поля discount нет то вывести просто поле price в консоль.

if ('discount' in productItem && parseInt(productItem.discount) > 0) {
	productItem.priceWithDiscount = parseFloat(productItem.price, 2) - parseFloat(productItem.price, 2) * parseInt(productItem.discount)/100 + '$';
	console.log(productItem.priceWithDiscount);
} else {
	console.log(productItem.price);
}

//6. Дан следующий код:
let product = {
    name: 'Яблоко',
    price: '30.5$'
};

let min = 10; // минимальная цена
let max = 20; // максимальная цена

//Написать условие если цена товара больше или равна минимальной цене и меньше или равна максимальной цене,
// то вывести в консоль название этого товара, иначе вывести в консоль что товаров не найдено.
if (parseFloat(product.price, 2) >= min && parseFloat(product.price, 2) <= max) {
	console.log(product.name);
} else {
	console.log('Товаров не найдено');
}
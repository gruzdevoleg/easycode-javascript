import { LoginComponent } from './components/login.component';
import { SignupComponent } from './components/signup.component';
import { HomeComponent } from './components/home.component';
import { NotFoundComponent } from './components/notfound.component';
import { UserComponent } from './components/user.component';
import { ActiveRoute } from './core/active-route.service';
import { NewsComponent } from './components/news.component'; 
//объект с возможными роутами
const routes = {
    '/': new HomeComponent(),
    '/login': new LoginComponent(),
    '/signup': new SignupComponent(),
    '/users/:id': new UserComponent(),
    '/news': new NewsComponent(),
    '**': new NotFoundComponent()//если любой другой путь
};
const activeRoute = new ActiveRoute();
//функция, которая в зависимости от роута вставляет соответствующий компонент
const router = async () => {
    const container = document.querySelector('app-container');
    
    //узнаем на каком урле находится пользователь
    //const url = location.hash.slice(1).toLowerCase() || '/';
    const request = activeRoute.parseRequestURL();
    const url = (request.resourse ? '/' + request.resourse : '/') + (request.id ? '/:id' : '');
    //берем из routes компонент, соответствующий текущему урлу
    const component = routes[url] || routes['**']; 
    await component.beforeRender();
    //и вставляем в контейнер на страницу через innerHTML контейнера
    //(метод render возвращает разметку)
    container.innerHTML = component.render();
    component.afterRender();
};

//вешаем эту функцию как обработчик на событие загрузки/перезагрузки страницы и изменение урла
window.addEventListener('load', router);
window.addEventListener('hashchange', router);
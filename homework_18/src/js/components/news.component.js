import { AuthService } from '../services/auth.service';

export class NewsComponent {
    constructor() {
        this._authService = new AuthService();
        this._news;
    }

    async beforeRender() {
  /**
   * перед рендерингом на страницу сохраняем то, с чем зарезолвился промис в свойство _news
 */
       this._news = await this._authService.news();
    }

    render() {
        return `
        <!-- Component styles -->
        <style>
            ${this._style()}
        </style>
        <!-- Component html -->
        <div class="news">
            <div class="author">
                <img src="${this._news.news[0].owner.avatar}"/>
                <p class="author__name">${this._news.news[0].owner.full_name}</p>
                <p class="author__country">${this._news.news[0].owner.country}</p>
            </div>
            <div class="content">
                <img src="${this._news.news[0].pictures[0].url}"/>
            </div>
        </div>
    `;
    }

    _style() {
        return `
            .news {
                display: flex;
                justify-content: space-between;
                font-family: 'Montserrat', sans-serif    
            }

            .author {
                width: 25%;
                text-align: center;

            }

            .author__name {
                font-size: 20px;
            }

            .author__country {
                font-size: 16px;
            }
        `;
    }

    afterRender() {

    }
}
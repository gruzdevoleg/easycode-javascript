
export class Routing {
/**
 * метод navigate - перенапрвляет на страницу пользователя, который залогинился
 * @param {String} route - путь на страницу пользователя  
 * @param {String} data - необязательный параметр
 * 
 */
    navigate(route, data = null) {
        location.appData = data;//подставляем data
        location.hash = route;//заменяем url на route
    }
}
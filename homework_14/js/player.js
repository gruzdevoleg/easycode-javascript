class VideoPlayer {
constructor(settings) {
    this._settings = Object.assign(VideoPlayer.DefaultSettings, settings);
}

init() {
    if (!this._settings.videoUrl) return console.error('Provide video Url');
    if (!this._settings.videoPlayerContainer) return console.error('Please provide video player container');
	if (typeof this._settings.skipBackTime !== 'number' || typeof this._settings.skipForwardTime !== 'number') {
		this._settings.skipBackTime = VideoPlayer.DefaultSettings.skipBackTime;
		this._settings.skipForwardTime = VideoPlayer.DefaultSettings.skipForwardTime;
	}
    // Создаем разметку для video
    this._addTemplate();
    // Найти все элементы управления
    this._setElements();
    // Установить обработчики событий
    this._setEvents();
}

toggle() {
    const method = this._video.paused ? 'play' : 'pause';
    this._toggleBtn.textContent = this._video.paused ? '❚ ❚' :  '►';
    this._video[method]();
}

_videoProgressHandler() {
    const percent = (this._video.currentTime / this._video.duration) * 100;
    this._progress.style.flexBasis = `${percent}%`;
}

_peremotka(event) {
    this._video.currentTime = (event.offsetX / this._progressContainer.offsetWidth) * this._video.duration;
}

_addTemplate() {
    const template = this._createVideoTemplate();
    const container = document.querySelector(this._settings.videoPlayerContainer);
    container ? container.insertAdjacentHTML('afterbegin', template) : console.error('Video container was not found');
}

//обработчик клика на volume
//Устанавливает ползунок в то место, куда кликнули, и изменяет свойство volume самого 
//объекта video в соответствии с текущим положением ползунка
_volumeRateClickHandler(event) {
	this._volume.value = (event.offsetX / this._volume.offsetWidth)/* * (this._volume.max - this._volume.min)*/;
	this._video.volume = +this._volume.value;
	console.log(this._volume.value);
	console.log(this._video.volume);
}

//обработчик клика на playbackRate
//Устанавливает ползунок в то место, куда кликнули, и изменяет свойство playbackRate самого 
//объекта video в соответствии с текущим положением ползунка
_playbackClickHandler(event) {
	this._playbackRate.value = +this._playbackRate.min + (event.offsetX / this._playbackRate.offsetWidth) * (+this._playbackRate.max - +this._playbackRate.min);
	this._video.playbackRate = +this._playbackRate.value;
}

//skip back and forward Handlers
_skipBackClickHandler() {
	console.log(this._video.currentTime - this._settings.skipBackTime);
	this._video.currentTime = this._video.currentTime - this._settings.skipBackTime;
}

_skipForwardClickHandler() {
	this._video.currentTime = this._video.currentTime + this._settings.skipForwardTime;
}

//перемотка вперед/назад при двойном клике на правую/левую часть видео
_videoSkipDoubleClickHandler(event) {
	this._video.removeEventListener('click', this._videoToggle);
	event.offsetX <= this._video.offsetWidth / 2 ? this._skipBackClickHandler() : this._skipForwardClickHandler();
	this._video.addEventListener('click', this._videoToggle);
}

//записываем в свойства класса DOM-элементы управления
_setElements() {
    this._videoContainer = document.querySelector(this._settings.videoPlayerContainer);
    this._video = this._videoContainer.querySelector('video');
    this._toggleBtn = this._videoContainer.querySelector('.toggle');
    this._progress = this._videoContainer.querySelector('.progress__filled');
    this._progressContainer = this._videoContainer.querySelector('.progress');
    this._volume = this._videoContainer.querySelector('input[name="volume"]');
    this._playbackRate = this._videoContainer.querySelector('input[name="playbackRate"]');
    this._skipBack = this._videoContainer.querySelector('button[data-skip="-1"]');
    this._skipForward = this._videoContainer.querySelector('button[data-skip="1"]');
}

//вешаем обработчики
_setEvents() {
	this._videoToggle = () => this.toggle();
    this._video.addEventListener('click', this._videoToggle);
    this._toggleBtn.addEventListener('click', () => this.toggle());
    this._video.addEventListener('timeupdate', () => this._videoProgressHandler());
    this._progressContainer.addEventListener('click', (e) => this._peremotka(e));
    this._volume.addEventListener('click', (e) => this._volumeRateClickHandler(e));
    this._playbackRate.addEventListener('click', (e) => this._playbackClickHandler(e));
    this._skipBack.addEventListener('click', () => this._skipBackClickHandler());
    this._skipForward.addEventListener('click', () => this._skipForwardClickHandler());
    this._video.addEventListener('dblclick', (e) => this._videoSkipDoubleClickHandler(e));
}

_createVideoTemplate() {
    return `
    <div class="player">
        <video class="player__video viewer" src="${this._settings.videoUrl}"> </video>
        <div class="player__controls">
          <div class="progress">
          	<div class="progress__filled"></div>
          </div>
          <button class="player__button toggle" title="Toggle Play">►</button>
          <input type="range" name="volume" class="player__slider" min=0 max="1" step="0.001" value="${this._settings.volume}">
          <input type="range" name="playbackRate" class="player__slider" min="0.5" max="2" step="0.1" value="1">
          <button data-skip="-1" class="player__button">« ${this._settings.skipBackTime}s</button>
          <button data-skip="1" class="player__button">${this._settings.skipForwardTime}s »</button>
        </div>
  </div>
    `;
}

static get DefaultSettings() {
    return {
        videoUrl: '',
        videoPlayerContainer: 'body',
        volume: .5,
        skipBackTime: 1,
        skipForwardTime: 1
    }
}
}


const playerInstance = new VideoPlayer({
videoUrl: 'video/mov_bbb.mp4',

});

playerInstance.init();

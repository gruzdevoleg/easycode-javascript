//1 Записать в виде switch case следующее условие:
/*if (a === ‘block’) {
	console.log(‘block’)
} else if (a === ‘none’) {
	console.log(‘none’)
} else if (a === ‘inline’) {
console.log(‘inline’)
} else {
	console.log(‘other’)
}*/
//Записать условие, используя конструктор switch.
// В консоли должно отразиться только одно значение.
let a = 'inline';
switch (a) {
  case 'block':
    console.log('block');
    break;	
  case 'none':
    console.log('none');
	break;
  case 'inline':
    console.log('inline');
    break;
  default:
    console.log('other');
}

//2 Из задач по условному оператору if else выполнить задачи 1, 2 и 3 в виде тернарного оператора.
//Если переменная равна “hidden”, присвоить ей значение “visible”, иначе - “hidden”.
let x = 5;
x = (x === 'hidden') ? 'visible' : 'hidden';
console.log(x);

 //если переменная равна нулю, присвоить ей 1;
//если меньше нуля - строку “less than zero”;
//если больше нуля - используя оператор “присвоение”, переменную умножить на 10 (использовать краткую запись).
let b = -2;
b = (b === 0) ? 1 : (b < 0) ? 'less than zero' : b *= 10;
console.log(b);

//3. Дан объект 
let car = {
	name: 'Lexus',
	age: 10,
	create: 2008,
	needRepair: false
};
//Написать условие если возраст машины больше 5 лет то нужно свойство 
//needRepair в объекте car изменить на true; иначе изменить на false.

(car.age > 5) ? car.needRepair = true : car.needRepair = false;
console.log('Need Repair');
	
//1. На основе строки “i am in the easycode” сделать новую строку,
// где первые буквы каждого слова будут в верхнем регистре. Использовать for или while.

let str = 'i am in the easycode';
let newStr = '';

for (let i = 0; i < str.length; i++) {
	newStr = (i === 0 || str[i-1] === ' ') ? newStr + str[i].toUpperCase() : newStr + str[i];
}

console.log(newStr);

//2. Дана строка “tseb eht ma i”. Используя циклы, сделать
//строку-перевертыш (то есть последняя буква становится первой, предпоследняя - второй итд).

let string = 'tseb eht ma i';
let reverseString = '';

for (let i = string.length-1; i >= 0; i--) {
	reverseString = reverseString + string[i];
}

console.log(reverseString);

//3. Факториал числа - произведение всех натуральных чисел от 1 до n
//включительно: 3! = 3*2*1, 5! = 5*4*3*2*1. 
//С помощью циклов вычислить факториал числа 10. Использовать for.

let value = 10;
let fact = 1;
for (let i = 1; i <= value; i++) {
	fact = fact*i;
}
console.log(fact);

//4. На основе строки “JavaScript is a pretty good language” сделать новую строку,
//где каждое слово начинается с большой буквы, а пробелы удалены. Использовать for.
let anyString = 'JavaScript is a pretty good language';
let transformString = '';

for (let i = 0; i < anyString.length; i++) {
	transformString = (i === 0 || anyString[i-1] === ' ') ? 
		transformString + anyString[i].toUpperCase() : 
		(anyString[i] === ' ') ? transformString :
	  	transformString + anyString[i];
}

console.log(transformString);

//5. Найти все нечетные числа от 1 до 15 включительно и вывести их в консоль.
//Использовать for of.
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
let oddNumbers = [];
let k = 0;

for (let num of numbers) {
	if (num % 2) {
		oddNumbers[k] = num;
		k++;
	}
}

console.log(oddNumbers);

//6. Дан объект:
let list = {
     name: 'denis',
     work: 'easycode',
     age: 29
}
//Перебрать объект и если значение в свойстве это строка, то переписать 
//ее всю в верхнем регистре. Использовать for in.
for (key in list) {
	if (typeof (list[key]) === 'string') {
		list[key] = list[key].toUpperCase();
	}
}

console.log(list);
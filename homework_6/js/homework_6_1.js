//Зная структуру html, с помощью изученных
//методов получить (в консоль):
//1. head
console.log(document.head);
//2. body
console.log(document.body);
//3. все дочерние элементы body и вывести их в
//консоль.
console.log(document.body.children);
//4. первый div и все его дочерние узлы
const firstDiv = document.body.children[0];
const firstDivChildNodes = document.body.children[0].childNodes;
//а) вывести все дочерние узлы в консоль
console.log(firstDivChildNodes);
//б) вывести в консоль все дочерние узлы,
//кроме первого и последнего
for (let node of firstDivChildNodes) 
	if (node !== firstDiv.firstChild && node !== firstDiv.lastChild)
		console.log(node);
		
	

//Для навигации по DOM использовать методы,
//которые возвращают только элементы

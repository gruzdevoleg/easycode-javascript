//1. Создать функцию, которая принимает два элемента. Функция проверяет,
// является ли первый элемент родителем для второго:

const isParent = (parent, child) => {
	let parentElem = child.parentElement;
	while (parent !== parentElem && parentElem !== null) {
		parentElem = parentElem.parentElement;
	}

	(parentElem !== null) ? console.log('true') : console.log('false');
}

isParent(document.body.children[0], document.querySelector('mark'));
// true так как первый див является родительским элементом для mark

isParent(document.querySelector('ul'), document.querySelector('mark'));
// false так ul НЕ является родительским элементом для mark

//2. Получить список всех ссылок, которые не находятся внутри списка ul
const allLinks = document.links;
//const allLinks = document.querySelectorAll('a');
for (let li of allLinks) {
	if (li.closest('ul') === null) console.log(li);
}
//3. Найти элемент, который находится перед и после списка ul
const list = document.querySelector('ul');
console.log(list.previousElementSibling);
console.log(list.nextElementSibling);

//4. Посчитать количество элементов li в списке
 const elementsInList = document.querySelector('ul').children;
 let liElementsQuantity = 0;
 for (let li of elementsInList) {
 	liElementsQuantity++;
 }
 console.log(liElementsQuantity);
//5. В коде с занятия написать функцию по редактированию задачи. 
//см. TODOS/js/todo_app.js
//6. Подумать и улучшить функцию generateId();
//см. TODOS/js/todo_app.js
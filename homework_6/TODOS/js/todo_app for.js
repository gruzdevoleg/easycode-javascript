/**
 * TODOS
 * 1. Добавление задачи
 * 2. Удаление задачи
 * 3. Редактирование задачи
 */

 /**
 * Одна задача это объект из следующих полей
 * id - произвольная уникальная строка
 * title - заголовок задачи
 * text - текст задачи
 */


 /**
  * todosStorage - обьект для хранения всех todos
  */
 const todosStorage = {
     currentTodos: [],
     deletedTodos: []
 };

 /**
 * generateId - создает произвольную строку 
 * @returns {string} - новый id
 */
const generateId = () => {
    const uniqueValues = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    let longId = '';

    for (let char of uniqueValues) {
        longId += uniqueValues[Math.floor(Math.random() * uniqueValues.length)];
    }

	  let trimId = longId.substr(0, 10);

    return trimId;
};

/**
 * 
 * @param {String} title 
 * @param {String} text
 * @returns {[]} currentTodos
 */
 const addTodoItem = (title, text) => {
    if (!title) return console.log('Please provide todo title');
    if (!text) return console.log('Please provide todo text');

    todosStorage.currentTodos.push({title, text, id: generateId()});
    return todosStorage.currentTodos;
 };
 

/**
 * 
 * @param {String} id 
 * @returns {[]} currentTodos
 */
const deleteTodoItem = (id) => {
    if (!id) return console.log("Передайте id удаляемой задачи.");
    
    todosStorage.currentTodos = todosStorage.currentTodos.filter((todoItem) => todoItem.id !== id);
    return todosStorage.currentTodos;
};

/**
 * 
 * @param {String} id 
 * @param {String} title
 * @param {String} text
 * @returns {[]} currentTodos с измененной задачей
 */

const editTodoItem = (id, title, text) => {
	let i;
	
	if (!id) return console.log('Передайте id редактируемой задачи.');
	if (!title) return console.log('Передайте новое название редактируемой задачи.');
  if (!text) return console.log('Передайте новое содержание для редактируемой задачи.');
  
	for (i = 0; i < todosStorage.currentTodos.length; i++) {
		if (todosStorage.currentTodos[i].id === id) {
			todosStorage.currentTodos[i].title = title;
			todosStorage.currentTodos[i].text = text;
			break;
		} 
	}
	
	return (i !== todosStorage.currentTodos.length) ? 
    todosStorage.currentTodos :
    'Задача с искомым Id не найдена';
};


//1.Переделать функцию с использованием функции-стрелки (в методе reduce тоже использовать arrow function):

/*function sum() {
  const params = Array.prototype.slice.call(arguments);
  if (!params.length) return 0;
  return params.reduce(function (prev, next) { return prev + next; });
}

sum(1, 2, 3, 4); // 10
sum(); // 0*/

const sum = (...rest) => {
	const params = Array.prototype.slice.call(rest);//берем из прототипа класса Array метод slice и вызываем его в нужном нам контексте
	if (!params.length) return 0;
	return rest.reduce((prev, next) => prev + next);
};

console.log(sum(1, 2, 3, 4)); // 10
console.log(sum()); // 0

//2. Создать объект, который описывает ширину и высоту
//прямоугольника, а также может посчитать площадь фигуры:
const rectangle = {
	width: 5,
	height: 10,
	getSquare: function() {
		return (typeof this.width === 'number' && typeof this.height === 'number') ? this.width * this.height : 'Введите корректные данные';
	}
};

console.log(rectangle.getSquare()); // 50

//3. Создать объект, у которого будет цена товара и его скидка, а также
//два метода: для получения цены и для расчета цены с учетом скидки:
/*const price = {
    price: 10,
    discount: '15%',
... };
price.getPrice(); // 10
price.getPriceWithDiscount(); // 8.5*/
const product = {
    price: 10,
    discount: '15%',
	getPrice: function() {
		return this.price;
	},
	getPriceWithDiscount: function() {
		return this.price - this.price * parseFloat(this.discount, 1)/100;
	}
};
console.log(product.getPrice()); // 10
console.log(product.getPriceWithDiscount()); // 8.5

//4.Создать объект, у которого будет поле высота и метод “увеличить
//высоту на один”. Метод должен возвращать новую высоту:
//object.height = 10;
//object.inc(); // придумать свое название для метода
//object.height; // 11;
const figure = {
	height: 10,
	increaseHeight: function() {
		this.height = this.height + 1;
		return this.height;
	}
};
console.log(figure.increaseHeight()); // 11
console.log(figure.increaseHeight()); // 12
//5.Создать объект “вычислитель”, у которого есть числовое свойство
//“значение” и методы “удвоить”, “прибавить один”, “отнять один”.
//Методы можно вызывать через точку, образуя цепочку методов:
//numerator.double().plusOne().plusOne().minusOne();
//numerator.value //3
const numerator = {
    value: 1,
    double: function() {
		this.value = this.value * 2;
		return this;
	},
    plusOne: function() {
		this.value = this.value + 1;
		return this;
	},
    minusOne: function() {
		this.value = this.value - 1;
		return this;
	}
};
numerator.double().plusOne().plusOne().minusOne();
console.log(numerator.value);

//6.Создать объект с розничной ценой и количеством продуктов. Этот
//объект должен содержать метод для получения общей стоимости
//всех товаров (цена * количество продуктов)
const productsStorage = {
	productPrice: '120$',
	productsQuantity: 12,
	totalPrice: function() {
		return parseFloat(this.productPrice, 2) * this.productsQuantity + '$';
	}
};
console.log(productsStorage.totalPrice());
//7. Создать объект из предыдущей задачи. Создать второй объект,
//который описывает количество деталей и цену за одну деталь. Для
//второго объекта нужно узнать общую стоимость всех деталей, но
//нельзя создавать новые функции и методы. Для этого
//“позаимствуйте” метод из предыдущего объекта.
const details = {
	productPrice: '12.5$',
	productsQuantity: 123
};
details.totalPrice = productsStorage.totalPrice.bind (details);
console.log(details.totalPrice());
//8 Даны объект и функция:
let sizes = {
	width: 5,
	height: 10
	};
let getSquare = function() {return this.width * this.height};
//Не изменяя функцию или объект, получить результат функции
//getSquare для объекта sizes
console.log(getSquare.call(sizes));

//9.  
let element = {
    height: 25,
    getHeight: function() {return this.height;}
};

let getElementHeight = element.getHeight;
getElementHeight(); // undefined

//Измените функцию getElementHeight таким образом, чтобы можно
//было вызвать getElementHeight() и получить 25.
getElementHeight = element.getHeight.bind(element);
console.log(getElementHeight());

//10. Используя rest оператор и деструктуризацию, создать функцию, которая принимает
//любое количество аргументов и возвращает объект, содержащий первый аргумент и массив из остатка:
const func = (arg_1, ...rest) => ({first: arg_1, other: rest});
console.log(func(1, 2, 3, 4));

//11. Организовать функцию getInfo, которая принимает объект вида
//{ name: ...,  info: { employees: [...], partners: [ … ]  } }
//и выводит в консоль имя (если имени нет, показывать ‘Unknown’) и первые две компании из массива partners:

const organization = {  
  name: 'Google',   
  info: { 
	employees: ['Vlad', 'Olga', 'Elena', 'Igor'],
	partners: ['Microsoft', 'Facebook', 'Xing']
	} 
};

const getInfo = ({name = 'Unknown', info: {employees = 'No employees', partners = 'No partners'} = 'No info'} = {}) => {
	console.log(`Name: ${name}`);
	console.log(`Partners: ${partners.slice(0, 2).join(', ')}`);
};

getInfo(organization); 

//Name: Google 
//Partners: Microsoft Facebook


//1.По нажатию на кнопку "btn-msg" должен появиться алерт с тем текстом который
// находится в атрибуте data-text у кнопки.
const btnMsg = document.getElementById('btn-msg');
btnMsg.addEventListener('click', function() {
	alert(`${btnMsg.dataset.text}`);
});

//2. При наведении указателя мыши на "btn-msg", кнопка становится красной;
// когда указатель мыши покидает кнопку, она становится прежнего цвета.
// Цвет менять можно через добавление класса.
btnMsg.addEventListener('mouseover', function() {
	btnMsg.classList.add('btn_red');
});
btnMsg.addEventListener('mouseout', function() {
	btnMsg.classList.remove('btn_red');
});
//3. При нажатии на любой узел документа показать в элементе с id=tag имя тега
// нажатого элемента.
document.addEventListener('click', function(event) {
	document.getElementById('tag').textContent = `Tag: ${event.target.tagName}`;
});
//4. При нажатии на кнопку btn-generate добавлять в список ul элемент списка Li
// с текстом Item + порядковый номер Li по списку, т.е Item 3, Item 4 и т.д 
const btnGnt = document.getElementById('btn-generate');
const list = document.querySelector('ul');

btnGnt.addEventListener('click', function() {
	const newItem = document.createElement('li');
	list.appendChild(newItem);
	newItem.textContent = `Item ${list.children.length}`;
});
//5. Из проекта todo реализовать редактирование задачи.
// (На занятии в конце второй части было пояснение).


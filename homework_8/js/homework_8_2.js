//7. Реализовать примитивный дропдаун. Изначально все dropdown-menu скрыты
//через класс .d-none. При клике на dropdown-item должен отображаться блок
//dropdown-menu который вложен именно в тот  dropdown-item на котором произошел клик.
//При повторном клике на этот же dropdown-item блок dropdown-menu должен закрыться.
//При клике на любой другой dropdown-item уже открытый dropdown-menu должен закрываться
//а на тот который кликнули открываться. 
const menu = document.querySelector('.menu');
const menuItems = menu.children;
const dropdownMenu = (event) => {
	let target = event.target;
		
	while (target.tagName !== 'LI') {
		target = target.parentElement;
	}

	if (target.classList.contains('dropdown-item')) {
		target.querySelector('.dropdown-menu').classList.toggle('d-none');
	
		for (let item of menuItems)
			if (item !== target && item.classList.contains('dropdown-item'))
				item.querySelector('.dropdown-menu').classList.add('d-none');
	}
};

menu.addEventListener('click', dropdownMenu);
// Todo manager
// 1. создать задачу
//      а. обработка формы
//          - проверить данные перед добавлением (валидация)
//      б. добавить задачу в массив
//      в. добавить данные в таблицу
//      г. очистить форму
// 2. удалить задачу
//      а. подтверждение
//      б. удаление данных из таблицы
//      в. удаление данных из массива 
// 3. редактировать задачу 
//      а. взять данные из массива
//      б. поместить в форму 
//      в. обработать форму на редактирование
//          - валидация
//      г. обновить данные в массиве
//      д. обновить данные в таблице
//      е. очистить форму

    
const todosStorage = {
    todos: []
};

// UI Elements
const formCol = document.querySelector('.form-col');
const form = document.forms['addTodoForm'];//найдет форму с name='addTodoForm'
const table = document.querySelector('.table tbody');
const title = form.elements['title'];//найдет в указанной форме input с name='title'
const text = form.elements['text'];//найдет в указанной форме input с name='text'
const submitButton = form.querySelector('button[type="submit"]');
let taskToEdit;

// event handling
form.addEventListener('submit', (event) => {
    event.preventDefault();

    if (!title.value || !text.value) return alertMessage('alert-danger', 'Введите title и text');
    // если есть аттр data-task-id
    if (form.dataset.taskId) {
        //1. Берем id редактируемой задачи и новые значения полей title, text из формы.
        //Это будут аргументы для функции editTaskStorage
        const editTaskId = form.dataset.taskId;
        const newTitle = title.value;
        const newText = text.value;
        // вызываем функцию editTaskStorage
        editTaskStorage(editTaskId, newTitle, newText);
		alertMessage('alert-info', 'Задача изменена');
        // удаляем с формы атрибут data-task-id
        form.removeAttribute('data-task-id');
		// Возвращаем текст кнопки на "Add task"
		submitButton.textContent = 'Add task';
    } else {
        addNewTodoToStorage(title.value, text.value);
        alertMessage('alert-info', 'Задача добавлена успешно');
    }
	
	// очистка формы 
	form.reset();
});

table.addEventListener('click', (event) => {
    if (event.target.classList.contains('remove-todo')) {
        const id = event.target.closest('[data-id]').dataset.id;
        deleteTodoFromStorage(id);
        alertMessage('alert-info', 'Задача удалена успешно');
        return;
    }

    if (event.target.classList.contains('edit-todo')) {
        const id = event.target.closest('[data-id]').dataset.id;
        setFormtoEdit(id);
    }
});


// alert messages
/**
 * 
 * @param {String} className 
 * @param {String} message 
 */
function alertMessage(className, message) {
    removeAlert();

    const template = alertTemplate(className, message);
    formCol.insertAdjacentHTML('afterbegin', template);

    setTimeout(removeAlert, 2000);
}

function removeAlert() {
    const currentAlert = document.querySelector('.alert');
    if (currentAlert) formCol.removeChild(currentAlert);
}

   
/**
* generateId - создает произвольную строку 
* @returns {string} - новый id
*/
function generateId() {
    const uniqueValues = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    let id = '';

    for (let char of uniqueValues) {
        let index = Math.floor(Math.random() * uniqueValues.length);
        id += uniqueValues[index];
    }
 
    return id;
 }


/**
* addNewTodoToStorage - добавляет новый todo в storage а потом в view
* @param {String} title 
* @param {String} text
* @returns {[]} currentTodos
*/
function addNewTodoToStorage(title, text) {
    if (!title) return console.log('Please provide todo title');
    if (!text) return console.log('Please provide todo text');
 
    const newTodo = {title, text, id: generateId()}
    todosStorage.todos.push(newTodo);

    // Добавим в разметку
    addNewTodoToView(newTodo);

    return todosStorage.todos;
 };

 /**
* 
* @param {String} id 
* @returns {[]} currentTodos
*/
function deleteTodoFromStorage(id) {
    const checkIdRes = checkId(id);
    if (checkIdRes.error) return console.log(checkIdRes.msg);
    
    let removedTask;

    for (let i = 0; i < todosStorage.todos.length; i++) {
        if (todosStorage.todos[i].id === id) {
            removedTask = todosStorage.todos.splice(i, 1);
            break;
        }
    }

    // удаляем с разметки
    deleteTodoFromView(id);
    
    return removedTask;
}

/**
 * 
 * @param {String} id 
 */
function checkId(id) {
    if (!id) return { error: true, msg: 'Передайте id удаляемой задачи.' };

    const idIsPresent = todosStorage.todos.some((todo) => todo.id === id );
    if (!idIsPresent) return { error: true, msg: 'Задачи с таким id несуществует' };

    return { error: false, msg: '' };
}


// View functions

/**
 * 
 * @param {String} id 
 */
function deleteTodoFromView(id) {
    const target = document.querySelector(`[data-id="${id}"]`);
    target.parentElement.removeChild(target);
}

/**
 * 
 * @param {*} task 
 */
function addNewTodoToView(todo) {
    const template = todoTemplate(todo);
    table.insertAdjacentHTML('afterbegin', template);
}

/**
 * 
 * @param {*} todo 
 * todo {
 *  id: string;
 *  title: string;
 *  text: string;
 * }
 */
function todoTemplate(todo) {
    return `
        <tr data-id="${todo.id}"> 
            <td>${todo.title}</td>
            <td>${todo.text}</td>
            <td>
                <i class="fas fa-trash remove-todo"></i>
                <i class="fas fa-edit edit-todo"></i>
            </td>
        </tr>
    `;
}

/**
 * 
 * @param {String} className 
 * @param {String} message 
 */
function alertTemplate(className, message) {
    return `<div class="alert ${className}">${message}</div>`;
}

addNewTodoToStorage('My title 1', 'My text 1');


// Make editing work

/**
 * 
 * @param {String} id 
 * @param {String} title 
 * @param {String} text 
 */
function editTaskStorage(id, title, text) {
    //1. Меняем title и text редактируемой задачи в storage
    taskToEdit.title = title;
    taskToEdit.text = text;
    //2. Меняем title и text редактируемой задачи в таблице (в разметке)
    const target = document.querySelector(`[data-id="${id}"]`);
    target.firstElementChild.textContent = title;
    target.firstElementChild.nextElementSibling.textContent = text;
}

/**
 * 
 * @param {String} id 
 */
function setFormtoEdit(id) {
    // 1. Ищем нужную задачу в storage
    for (let task of todosStorage.todos)
        if (task.id === id) {
            taskToEdit = task;
            break;
        } 
    //В поле title и text записываем значение title, text с todo, которую мы получили
    //из storage
    title.value = taskToEdit.title;
    text.value = taskToEdit.text;
    // добавляем форме атрибут data-task-id=id;
    form.setAttribute('data-task-id', id);
    // получаем доступ к submit кнопке и перезаписываем ее на save
    submitButton.textContent = 'Save';
}